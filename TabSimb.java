import java.util.ArrayList;
import java.util.Iterator;

public class TabSimb
{
    private ArrayList<TS_entry> lista;
    
    public TabSimb( )
    {
        lista = new ArrayList<TS_entry>();
    }
    
    public void insert( TS_entry nodo ) {
		lista.add(nodo);
    }    
    
    public void listar() {
		int cont = 0;  
		for (TS_entry nodo : lista) {
			nodo.listar();
		}
    }
      
    public TS_entry pesquisa(String umId) {
		for (TS_entry nodo : lista) {
			if (nodo.getId().equals(umId)) {
				return nodo;
			}
		}
		return null;
    }

	 public TS_entry pesquisa(String umId, TabSimb valores) {
		for (TS_entry nodo : lista) {
			if (nodo.getId().equals(umId) && nodo.getParam().getLista().size() == valores.getLista().size()) {
				boolean achou = true;
				//System.out.println("achou");
				for (int i = 0; i < nodo.getParam().getLista().size(); i++) {
					//System.out.println("ID: " + umId + " a: " + nodo.getParam().getLista().get(i) + " b: " + valores.getLista().get(i));
					if (nodo.getParam().getLista().get(i).getId() != valores.getLista().get(i).getId() && nodo.getParam().getLista().get(i).getTipo() != valores.getLista().get(i).getTipo()) {
						achou = false;
					}
				}
				
				if (achou) {
					//System.out.println("achou mesmo");
					return nodo;
				}
			}
		}
		return null;
    }

    public  ArrayList<TS_entry> getLista() {
		return lista;
	}
}



