// Matheus Streb, 142021260, matheus.streb@acad.pucrs.br
// Vinicius Jacques, 11100168, viniciusjacques@gmail.com

%{
  import java.io.*;
%}
   

%token CLASS, PRIVATE, PUBLIC, INT, BOOLEAN, DOUBLE, STRING, VOID, IF, ELSE, ENDIF, FOR, ENDFOR, WHILE, ENDWHILE, BREAK, NUM, NUM_D, LEIA, ESCREVA, IDENT, RETURN, NEW, TRUE, FALSE, LITERAL, AND, OR, EQUALS, LESSEQUAL, GREATEREQUAL, INCREMENT, DECREMENT, EXTENDS

%right '='
%nonassoc '>', '<',  EQUALS, LESSEQUAL, GREATEREQUAL, INCREMENT, DECREMENT
%left '+', '-'
%left '*', '/'
%left AND, OR

%type <obj> VOID
%type <obj> type
%type <obj> exp
%type <obj> atribute
%type <sval> IDENT

%%

init: {currEscopo = new TS_entry("aJavaFile", null, ClasseID.Programa); topo = currEscopo;} class_list;

class_list: class_list class_init | class_init;

class_init : CLASS IDENT { 
							TS_entry nodo = ts.pesquisa($2);
							if (nodo != null) {
								yyerror("(sem) Classe <" + $2 + "> já foi declarada.");
							} else {
								nodo = new TS_entry($2, null, ClasseID.ClasseNome);
								currEscopo.insert(nodo);
								currEscopo = nodo;
								ts.insert(nodo);
							}
						} ext '{' class_declaration '}' {
							currEscopo = topo;
						}
	;
	
ext : EXTENDS IDENT {
						TS_entry nodo = ts.pesquisa($2);
						if (nodo == null || nodo.getClasse() != ClasseID.ClasseNome) {
							yyerror("(sem) Classe <" + $2 + "> não foi declarada.");
						} else {
							currEscopo.setPai(nodo);
						}
					}
	| ;

class_declaration : 	private_declaration public_declaration
	;

private_declaration : PRIVATE ':' { currClass = ClasseID.AtribClass; } atributes
	|
	;

public_declaration : PUBLIC ':' methods
	|
	;

atributes : atribute
	| atribute atributes
	;

methods	: method
	| method methods
	;

type : INT { $$ = Tp_INT; }
	| DOUBLE { $$ = Tp_DOUBLE; }
	| STRING { $$ = Tp_STRING; }
	| BOOLEAN { $$ = Tp_BOOL; }
	| IDENT { TS_entry nodo = ts.pesquisa($1);
            if (nodo == null ) 
              yyerror("(sem) Nome de tipo <" + $1 + "> não declarado ");
            else 
              $$ = nodo;
          } 
	;

atribute : type {
                  TS_entry nodo = ts.pesquisa( ((TS_entry)$1).getTipoStr() );
                  if (nodo == null ) 
                    yyerror("(sem) Nome de tipo <" + $1 + "> não declarado ");
                  else 
                    currType = nodo;
                } ident_list  ';' ;

ident_list : id | id ',' ident_list ;

id : IDENT {  TS_entry nodo = currEscopo.pesquisa($1);
              if (nodo != null) 
              	 yyerror("(sem) atributo >" + $1 + "< já declarado");
              else currEscopo.insert(new TS_entry($1, currType, currClass));
            };

method : method_type '(' parameters ')' local_var '{' cmd_list '}' { currEscopo = currEscopo.getPai(); } ;

method_type : IDENT { 	currType = currEscopo;
						currMethod = new TS_entry($1, currType, ClasseID.NomeMetodo, currEscopo);
					   }
				| type IDENT {  currType = ((TS_entry)$1);
								currMethod = new TS_entry($2, currType, ClasseID.NomeMetodo, currEscopo);
								}
 				| VOID IDENT {  currType = Tp_VOID;
								currMethod = new TS_entry($2, currType, ClasseID.NomeMetodo, currEscopo);	
								};

local_var : {currClass = ClasseID.VarLocal; } atributes | ;

parameters : parameter_list | {
								TS_entry nodo = currEscopo.pesquisa(currMethod.getId());
								if (nodo != null)
									yyerror("(sem) metodo >" + currMethod.getId() + "< já declarado");
								else {
									currEscopo.insert(currMethod);
									currEscopo = currMethod;
								}
							};

parameter_list : parameter {
							TS_entry nodo = currEscopo.pesquisa(currMethod.getId(), currMethod.getParam());
							if (nodo != null)
								yyerror("(sem) metodo >" + currMethod.getId() + "< já declarado");
							else {
								currEscopo.insert(currMethod);
								currEscopo = currMethod;
							}
						}
				| parameter ',' parameter_list ;

parameter : type IDENT { 	currType = ((TS_entry)$1);
									TS_entry nodo = currMethod.pesquisa($2);
									if (nodo != null)
										yyerror("(sem) parametro >" + $2 + "< já declarado");
									else {
										currMethod.insert(new TS_entry($2, currType, ClasseID.NomeParam));
									};
            				};

cmd_list : cmd cmd_list | ;

cmd : exp ';' | if_cmd | while_cmd | for_cmd  | return_cmd ';'  | escreva_cmd ';'  | leia_cmd ';';

return_cmd: RETURN exp  { 
                            if(currEscopo.getTipo() != ((TS_entry)$2))
                                yyerror("(sem) metodo >"+currEscopo.getId()+"< espera retorno do tipo >" + currEscopo.getTipo().getTipoStr() + "<");
                        } ;

escreva_cmd: ESCREVA exp | ESCREVA exp ',' exp ;

leia_cmd: LEIA exp;

values  : exp               { currValues.insert(new TS_entry(null, ((TS_entry)$1), ClasseID.NomeParam)); }
        | exp ',' values    { currValues.insert(new TS_entry(null, ((TS_entry)$1), ClasseID.NomeParam)); }
        |   
        ;

exp : exp '+' exp               { $$ = validaTipo('+', (TS_entry)$1, (TS_entry)$3); }
    | exp '-' exp               { $$ = validaTipo('-', (TS_entry)$1, (TS_entry)$3); }
    | exp '*' exp               { $$ = validaTipo('*', (TS_entry)$1, (TS_entry)$3); }
    | exp '/' exp               { $$ = validaTipo('/', (TS_entry)$1, (TS_entry)$3); }
    | exp '>' exp               { $$ = validaTipo('>', (TS_entry)$1, (TS_entry)$3); }
    | exp '<' exp               { $$ = validaTipo('<', (TS_entry)$1, (TS_entry)$3); }
    | exp EQUALS exp            { $$ = validaTipo(EQUALS, (TS_entry)$1, (TS_entry)$3); }
    | exp LESSEQUAL exp         { $$ = validaTipo(LESSEQUAL, (TS_entry)$1, (TS_entry)$3); }
    | exp GREATEREQUAL exp      { $$ = validaTipo(GREATEREQUAL, (TS_entry)$1, (TS_entry)$3); }
    | exp AND exp               { $$ = validaTipo(AND, (TS_entry)$1, (TS_entry)$3); }
    | exp OR exp                { $$ = validaTipo(OR, (TS_entry)$1, (TS_entry)$3); }
    | '(' exp ')'               { $$ = $2; }
    | NUM                       { $$ = Tp_INT; }
    | NUM_D                     { $$ = Tp_DOUBLE; }
    | LITERAL                   { $$ = Tp_STRING; }
    | IDENT                     {   TS_entry nodo = currEscopo.pesquisaRec($1);
                                    if(nodo == null)
                                        yyerror("(sem) variavel >" + $1 + "< não declarada");
                                    $$ = nodo.getTipo(); }
    | exp INCREMENT             {
                                    if((TS_entry)$1 != Tp_INT && (TS_entry)$1 != Tp_DOUBLE)
                                        yyerror("(sem) variavel >" + $1 + "< não é número");
                                    $$ = $1;
                                }
    | exp DECREMENT             {
                                    if((TS_entry)$1 != Tp_INT && (TS_entry)$1 != Tp_DOUBLE)
                                        yyerror("(sem) variavel >" + $1 + "< não é número");
                                    $$ = $1;
                                }     
    | TRUE                      { $$ = Tp_BOOL; }
    | FALSE                     { $$ = Tp_BOOL; }
    | exp '=' exp               { $$ = validaTipo('=', (TS_entry)$1, (TS_entry)$3); }
    | NEW IDENT                 {
                                    currValues = new TabSimb();
                                } '(' values ')'  { 
                                  TS_entry nodo = ts.pesquisa($2, currValues);
                                  if(nodo == null)
                                     yyerror("(sem) classe >" + $2 + "< não declarada");
                                  $$ = nodo;
										            }
    | IDENT                     {
                                    currValues = new TabSimb();
                                } '(' values ')' {
                                    TS_entry nodo = currEscopo.pesquisaRec($1, currValues);
                                    if(nodo == null)
                                        yyerror("(sem) metodo >" + $1 + "< não declarado");
                                    $$ = nodo.getTipo();
                                }
    | IDENT '.' IDENT           {
                                    currValues = new TabSimb();
                                } '(' values ')' {
                                    TS_entry nodo = currEscopo.pesquisaRec($1);
                                    if(nodo == null)
                                        yyerror("(sem) objeto >" + $1 + "< não declarado");
                                    TS_entry nodo2 = currEscopo.pesquisaRec($3, currValues);
                                    if(nodo2 == null)
                                        yyerror("(sem) objeto >" + $3 + "< não declarado");
                                    $$ = nodo2.getTipo();
                                }                                  
    ;

if_cmd: IF exp ':' cmd_list else_cmd ENDIF  {
                                                if((TS_entry)$2 != Tp_BOOL)
                                                    yyerror("(sem) if: expressão não é valor lógico");
                                            };

else_cmd: ELSE ':' cmd_list | ;

while_cmd: WHILE exp ':' loop_cmd_list ENDWHILE {
                                                    if((TS_entry)$2 != Tp_BOOL)
                                                        yyerror("(sem) if: expressão não é valor lógico");
                                                };

loop_cmd_list: loop_cmd loop_cmd_list | ;

loop_cmd: cmd | BREAK ;

for_cmd: FOR exp ';' exp ';' exp ':' loop_cmd_list ENDFOR   {
                                                                if((TS_entry)$4 != Tp_BOOL)
                                                                    yyerror("(sem) if: expressão não é valor lógico");
                                                            };

%%

  private Yylex lexer;
  private TabSimb ts;
  private TabSimb currValues;
   
  public static TS_entry Tp_VOID =  new TS_entry("void", null, ClasseID.TipoBase);
  public static TS_entry Tp_INT =  new TS_entry("int", null, ClasseID.TipoBase);
  public static TS_entry Tp_DOUBLE = new TS_entry("double", null,  ClasseID.TipoBase);
  public static TS_entry Tp_BOOL = new TS_entry("bool", null,  ClasseID.TipoBase);
  public static TS_entry Tp_STRING= new TS_entry("string", null,  ClasseID.TipoBase);
  public static TS_entry Tp_ERRO = new TS_entry("_erro_", null,  ClasseID.TipoBase);

  private TS_entry topo;
  private TS_entry currEscopo;
  private ClasseID currClass;
  private TS_entry currType;
  private TS_entry currMethod;

  private int yylex () {
    int yyl_return = -1;
    try {
      yylval = new ParserVal(0);
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
  }


/* metodo de manipulacao de erros de sintaxe */  
public void yyerror (String error) {   
   System.err.println("Erro : " + error + " na linha " + lexer.getLine());   
   System.err.println("Entrada rejeitada");  
} 



  public Parser(Reader r) {
    lexer = new Yylex(r, this);

    ts = new TabSimb();

    //
    // não me parece que necessitem estar na TS
    // já que criei todas como public static...
    //
    ts.insert(Tp_VOID);
    ts.insert(Tp_ERRO);
    ts.insert(Tp_INT);
    ts.insert(Tp_DOUBLE);
    ts.insert(Tp_STRING);
    ts.insert(Tp_BOOL);
  }

  TS_entry validaTipo(int operador, TS_entry A, TS_entry B) {
    switch ( operador ) {
        case '=':
              if ((A == Tp_INT && B == Tp_INT) || ((A == Tp_DOUBLE && (B == Tp_INT || B == Tp_DOUBLE))) || (A == B))
                    return A;
                else
                    yyerror("(sem) tipos incomp. para atribuicao: "+ A.getTipoStr() + " = "+B.getTipoStr());
              break;
        
        case '-' :
        case '+' :
        case '*' :
        case '/' :
            if(operador == '+' && (A == Tp_STRING || B == Tp_STRING)){
                return Tp_STRING;
            }
        
            if ( A == Tp_INT && B == Tp_INT)
                return Tp_INT;
            else if ( (A == Tp_DOUBLE && (B == Tp_INT || B == Tp_DOUBLE)) || (B == Tp_DOUBLE && (A == Tp_INT || A == Tp_DOUBLE)) ) 
                return Tp_DOUBLE;     
            else
                yyerror("(sem) tipos incomp. para operacao: "+ A.getTipoStr() + " " + operador + " " + B.getTipoStr());
                break;
        case LESSEQUAL:
        case GREATEREQUAL:
        case '<' :
        case '>' :
                if ((A == Tp_INT || A == Tp_DOUBLE) && (B == Tp_INT || B == Tp_DOUBLE))
                    return Tp_BOOL;
                else
                  yyerror("(sem) tipos incomp. para op relacional: "+ A.getTipoStr() + " " + operador + " "+B.getTipoStr());
                break;

        case AND:
        case OR:
                if (A == Tp_BOOL && B == Tp_BOOL)
                    return Tp_BOOL;
                else
                  yyerror("(sem) tipos incomp. para op lógica: "+ A.getTipoStr() + " " + operador + " "+B.getTipoStr());
            break;
      }

      return Tp_ERRO;
           
 }

public void listarTS() { topo.listar();}

public static void main(String args[]) throws IOException {
    System.out.println("\naJava - exemplo de uso do byacc");

    Parser yyparser;
    if ( args.length > 0 ) {
      // parse a file
      yyparser = new Parser(new FileReader(args[0]));

      yyparser.yyparse();
 
      System.out.println("\nFeito!!!");
		System.out.println("\n\nListagem da tabela de simbolos:\n");
      yyparser.listarTS();
    }
    else {
      System.out.println("Uso:  java Parser <arquivo de teste>");
    }
}
