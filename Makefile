# Matheus Streb, 142021260, matheus.streb@acad.pucrs.br
# Vinicius Jacques, 111001681, viniciusjacques@gmail.com

JFLEX  = java -jar JFlex.jar
BYACCJ = ./yacc.linux -tv -J
JAVAC  = javac -cp .

all: Parser.class

run: Parser.class
	java Parser

build: clean Parser.class

clean:
	rm -f *~ *.class y

Parser.class: TS_entry.java TabSimb.java Yylex.java Parser.java
	$(JAVAC) Parser.java

Yylex.java: lexico.flex
	$(JFLEX) lexico.flex

Parser.java: sintatico.y
	$(BYACCJ) sintatico.y
